+++
title = "Abitur: Kritik einer Schulkultur"
date = "2017-05-19T08:26:55+02:00"
tags = ["Montagsmotzi", "Satire"]
description = "Geschützt durch die Anonymität schreibt das Montagsmotzi auch über die unangenehmen Seiten der Schule."
+++

Nun ja, es ist wieder soweit. Das Abitur stand vor der Tür. Vor nicht allzu langer Zeit konnte man die Phänomene der schwindenden Tische, der fehlenden Lehrer und der unbenutzbaren Turnhalle beobachten. Schüler verschiedener Altersklassen, die froh waren, sich nicht demnächst in einer stickigen Halle zusammen mit 130 anderen, armen Abiturienten links und rechts von sich, den Prüfungen stellen zu müssen, schleppten Tische von A nach B und von dort erneut nach A, nur um sie dann an einem ganz anderen Ort zu platzieren. Aber was tut man nicht alles für die gebeutelten Abiturienten, die sowieso 24/7 mit Lernen beschäftigt sind/sein sollten? Schließlich würden die Jahrgänge nach uns dies auch für uns tun. Oder?

Von völlig tiefenentspannt bis hochnervös schwitzten die Abiturienten über ihren Prüfungen, immer das Ziel des bald endenden Schuljahres vor Augen.

Nun, nachdem das Gröbste überstanden ist und das Zittern um die Ergebnisse losgeht, genießt der eine oder andere die freie Zeit und garantiert niemand frönt dem Nichtstun oder gar dem Alkohol.

Mit einem breiten Grinsen im Gesicht, aber hoffentlich auch mit einem kleinen wehmütigen Stich, verlassen uns auch dieses Jahr wieder die (hoffentlich) Ärzte, Juristen, Lehrer, Youtuber, Zebrastreifenzähler, Weltverbesserer, DJs, Barkeeper und was man sich sonst noch so an sinnvollen Jobs vorstellen kann, von morgen. 

Eine bei Klausuren lärmende Dauerbaustelle, einen roten Pfeil, den keiner versteht, Wurstwürfel in Tomatensoße, einen Ticker, den anscheinend niemand liest, nicht funktionierende Computer, AC und weitere Buchstaben; all das werden Dinge sein, die ihnen im Studium wohl so schnell nicht wieder begegnen werden. All diese Dinge haben die Schulzeit der beinahe Erwachsenen besonders und einzigartig gemacht und sind hoffentlich Dinge, an die sie sich nicht nur mit dem blanken Grauen erinnern werden.

Wir, die Schülerzeitung, aber ich spreche wohl im Namen der gesamten Schülerschaft, wünschen ihnen auf ihrem weiteren Weg alles, alles Gute und hoffentlich leisere Baustellen im Studium!

Macht’s gut und auf ein Neues!

*Das Montagsmotzi schreibt Satire über Schulkultur für die Tafelrunde.*
