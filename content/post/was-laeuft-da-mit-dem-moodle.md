+++
title = "Was läuft da mit dem Moodle?"
date = "2017-04-30T21:25:37+02:00"
description = "Ein Moodle ist in der Mache. Was? Eine Nudel?"
tags = ["Projekte", "Schüler über Schüler", "Schulpolitik"]
+++

Unsere Schule besitzt seit einiger Zeit ein Moodle. Eine Gruppe von Schülern um Herrn Hollmann hat sich nun vorgenommen dem Moodle neues Leben einzuhauchen.

# Was ist das eigentlich?

Moodle ist eine Online-Platform für interaktives Lernen. Im Zentrum stehen zwei Dinge: Lerninhalte und Kurse.

Lerninhalte können erstellt oder verlinkt werden. So können Schüler und Lehrer auf Informationstexte, Beispielaufgaben, Videos und vieles mehr zugreifen.

Außerdem können Kurse verwaltet werden. Ein Kurs ist nichts anderes als eine Gruppe von Leuten; einer davor ist der Kursleiter. Dieser Kurs hat dann einen gemeinsamen Platz im Moodle: für Nachrichten, Termine oder das einreichen von Hausaufgaben und vieles mehr.

# Was haben wir vor?

Das Ziel ist es nun dieses System in der Schule einzuführen. Da steckt aber noch viel Arbeit drin.

Unsere größte Aufgabe ist es eine reiche Sammlung von Lerninhalten zu erstellen. Diese werden am Lehrplan orientiert sein. Unser Ziel ist es, für jedes Fach und jede Klasse Material zur Verfügung zu stellen, dass den ganzen Pflichtstoff begleitet.

Darüber hinaus müssen natürlich die Kursstrukturen geschaffen werden. Zunächst werden wir in einzelnen Kursen eine Testphase anlaufen lassen. Begleitend dazu werden wir Materialien zur Benutzung und Einführung ins Moodle erstellen. Diese orientieren sich an den Problemen, die in den Testkursen auftreten.

# Kann man da helfen?

Ja, auf jeden Fall. Wir suchen immer engagierte Schüler. Besonders viel Hilfe brauchen wir noch bei der Erarbeitung der Lerninhalte, aber auch andere Talente kannst du bei uns einbringen.

Sprich einfach Herrn Hollmann oder mich an. Wir erklären dir gerne alles genau. Mich erreichst Du außerdem unter *ben.sv-rostock@cjd-nord.de*.

*Es berichtete Ben J. Bals, Mitglied im Moodle-Team.*
