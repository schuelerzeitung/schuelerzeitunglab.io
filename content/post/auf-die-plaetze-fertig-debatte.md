+++
date = "2017-04-30T21:22:47+02:00"
title = "Auf die Plätze, fertig, Debatte!"
description = "Jugend debattiert - ein neuer Wettbewerb - hat an der Schule Einzug gehalten. Eine Kandidatin des Landeswettbewerbs berichtet."
tags = ["Hautnah dabei","Schüler über Schüler"]
+++

Wie manch einer vielleicht mitbekommen hat, forscht oder musiziert die Jugend an unserer Schule seit diesem Schuljahr nicht mehr nur, sondern debattiert nun auch miteinander um die Wette. Und wie das so genau abläuft, will ich euch jetzt etwas näher bringen.

# Der Wettbewerb an sich

Der Wettbewerb „Jugend debattiert“ ist ein bundesweiter Wettbewerb, welcher von wichtigen Leuten, wie dem Bundespräsidenten, oder Stiftungen, wie der Hertie-Stiftung, der Robert Bosch-Stiftung, der Stiftung Mercator und der Heinz Nixdorf-Stiftung, gefördert und mittlerweile sogar auf internationaler Ebene ausgrichtet wird.

Aber bis man so weit kommt, beginnt man mit der einfachen Arbeit im Unterricht, bei uns an der Schule lernt ihr das Debattieren in eurem Sozialkundeunterricht oder zum Beispiel auch im Wahlpflichtkurs kennen. Dann gibt es jeder Jahr einen schulinternen Ausscheid, wobei die beiden Besten in jeder Altersstufe (also Sekundarstufe I und II) weiter kommen zum nächsten Ausscheid, der pro Regionalverband ausgetragen wird. Wir gehören, wer hätte das gedacht, zum Regionalverbund Rostock, in dem auch andere Schulen vertreten sind, wie zum Beispiel die Werkstattschule oder das ISG. 

Nachdem unter den vier Besten ein Finale ausgetragen wurde, rücken dann immer die zwei Besten eine Stufe weiter, wobei man, bevor man dann am Landesfinale teilnimmt, noch die Chance hat, am Landessiegerseminar teilzunehmen, um sich vorzubereiten.
Auch beim Landesfinale, welches im Schweriner Schloss stattfindet, muss man wieder in zwei Vorrunden sich, zusammen mit drei anderen, für das Finale qualifizieren, wo erneut zwei Sieger pro Altersstufe ermittelt werden. Diese fahren dann zum Bundessiegerseminar, als Vorbereitung für das Bundesfinale, wo dann die vier besten aus jedem Bundesland, also die vier besten jeder Altersstufe, über mehrere Tage hinweg debattieren.

Die vier besten Debattanten und Debattantinnen aus ganz Deutschland dürfen dann vor u. a. dem Bundespräsidenten die Finaldebatte austragen, wo dann der Bundessieger oder die Bundessiegerin gekürt wird.

# Wie ist eine Debatte aufgebaut?

Man darf sich unter so einer Debatte definitiv nichts ähnlich einer Talk-Show aus dem Fernsehen oder einer mitgeschnittenen Rede aus dem Land- oder Bundestag vorstellen, da man an eine feste Struktur gebunden ist, die folgendermaßen aussieht:

Zwei Schüler je Seite, also Pro und Kontra, treten gegeneinander im Ping-Pong-Prinzip an. Dabei müssen sie zu einem bestimmten Thema, festgelegt in einer Streitfrage, debattieren. Die Debatte ist in drei Teile gegliedert: Die Einführungsrede, die freie Aussprache und die Schlussrede. Dabei beginnt Pro 1 mit der genauen Definition von Begriffen und stellt die Maßnahme der Pro-Seite vor, wie genau sie die geforderte Änderung zum Beispiel durchsetzen wollen. Dann kommt Kontra 1, dann Pro 2 und dann Kontra 2. Jeder hat für seinen Redebeitrag 2 Minuten Zeit. Somit dauert eine vollständige Debatte 24 Minuten.

Wenn alle Positionen ihre Aufgabe erfüllt haben, kommt es zur freien Aussprache, die insgesamt 12 Minuten dauert. Dabei können nun frei Argumente ausgetauscht, entkräftet oder komplett neue Gesichtspunkte zu diesem Thema beleuchtet werden. Eine feste Reihenfolge oder Zeitvorgabe für die Beiträge gibt es nicht.

Beendet wird die Debatte durch die einzelnen Schlussreden, die wieder im Ping-Pong-Prinzip gehalten werden (also Pro 1, dann Kontra 1, dann Pro 2 usw.). Jeder Redner hat dafür eine Minute Zeit, um zum Beispiel ein Fazit zu ziehen und noch einmal seine Position klar zu machen.

# Worauf kommt es an?

Natürlich gibt es wie bei jedem Wettbewerb bestimmte Regeln und Kriterien, nach denen bewertet wird. Diese sind: Sachkenntnis, Ausdrucksvermögen, Gesprächsfähigkeit und Überzeugungskraft. Es ist also nicht nur wichtig, was man sagt, sondern auch wie man es sagt. Man achtet zum Beispiel auch darauf, dass man miteinander debattiert und nicht gegeneinander oder die anderen ausreden lässt und ihnen nicht ins Wort fällt. 

In jeder Kategorie kann man bis zu 5 Punkten erreichen. Bewertet wird man dabei von einer Jury, die meistens 3 Mitglieder hat. Außerdem gibt es noch einen Zeitwächter, damit man die Zeitvorgaben auch einhält. Man wird jedoch 15 Sekunden vor Ablauf der Zeit mit einem kleinen Warnklingeln darauf vorbereitet, dass nun der nächste an der Reihe ist.

# Meine Erfahrungen

Ich selber wurde von diesem ganzen Wettbewerb mit seinen Regeln etwas überrumpelt, da er ziemlich neu bei uns an der Schule ist und der Schulausscheid für zumindest uns in der Sek II so gut wie unvorbereitet stattfand. So ging es dann auch für mich und die drei anderen (also Luis, Albert und Ludwig) zum Regionalwettbewerb hier bei uns an der Schule, für Luis und mich dann sogar noch einen Tag später zum Finale des Verbandes ans ISG, bei dem ich Zweite und er Vierter wurde. 

Ein wenig überstürzt hieß es dann also, dass es für mich nächste Woche zum Landessiegerseminar nach Schwerin geht. Dort haben wir, also die anderen Regionalsieger und ich, 3 Tage lang einen regelrechten Crash-Kurs bekommen, was das Debattieren anging, aber auch außerhalb hatten wir ziemlich viel Spaß miteinander, sodass man dann mit gutem Gefühl zum Landesfinale ins Schloss fuhr, welches im Plenarsaal stattfand, also dort wo normalerweise MV’s Politik gemacht und eben auch ab und zu debattiert wird. Mit eher unglücklich gewählten Themen konfrontiert, waren wir nachher alle ziemlich nah beinander und den Einzug ins Finale dort verpasste ich mit einem Punkt. Nun also von den Zuschauerrängen aus erlebte ich die beiden Finaldebatten Sek I und Sek II und drücke nun Kim und Laura, wie auch Paula und Leo die Daumen beim Bundesfinale in Berlin, welches in diesen Tage stattfindet. 

*Es berichtete Lara Mixdorf, Kandidatin des Landeswettbewerbs.*
