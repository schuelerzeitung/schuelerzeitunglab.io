+++
date = "2017-04-30T21:11:39+02:00"
title = "Die Frage nach dem wirklich richtigen Beruf"
tags = ["Interview"]
description = "Die wohl wichtigste Frage. Ein Interview mit einer Klavierlehrerin über ihre Entscheidung."
+++

Um eine Antwort auf die Frage nach dem richtigen Job zu kennen, ist es wichtig, viel über die Berufe, die weniger im Fokus stehen, zu wissen. Oft kann man erst nach langer Berufserfahrung sagen, dass genau das , was man tut , für einen das Richtige ist. Trotzdem sind Berufe wie Klavierlehrerin besonders interessant und für viele auch eine Form, ihren Charakter und das, was man wirklich will, zu leben. 

Hier ein Interview mit Annette Hagelstange für alle, die auch auf die so oft gestellte Frage eine Antwort brauchen: „Was willst du denn später mal werden?“

**Tafelrunde: Wollten Sie schon immer Klavierlehrerin werden?**

Annette Hagelstange: Nein, ich wollte nicht von Anfang diesen Beruf lernen. Ich sah die Richtung Musik immer als Möglichkeit und habe erst nach längerer Zeit die Sicherheit gehabt, dass diese Arbeit perfekt für mich ist.

**Was mussten Sie tun, um Klavierunterricht am Konservatorium geben zu können?**

Als Hauptvoraussetzung natürlich Klavier spielen können und auch die Musiktheorie beherrschen. Die Aufnahmeprüfung des Studiums zu bestehen, war dann der größte Schritt zu diesem Beruf.

**Warum haben Sie sich für das Klavier entschieden und nicht für ein anderes Instrument?**

Es gab für mich nicht die Frage nach einem anderen Instrument: Ich begann mit dem Klavierspiel mit 5 Jahren, da meine Familie einen Klavierlehrer kannte und ich schon als kleines Kind musikalisch war und singend durch die Wohnung sprang.

**Welche Art von Musik gefällt ihnen besonders?**

Ich mag besonders romantische Musik von Schumann oder Chopin, weil sie eine wunderbare Harmonie enthält. Auch Barockmusik höre ich gern, aber spielen mag ich diese eher weniger.

**Was für Eigenschaften braucht man, wenn man Klavier unterrichtet?**

Man braucht einerseits die anatomischen Voraussetzungen fürs Klavier, um die Technik ohne wirkliche Komplikationen auszuführen, aber man sollte auf jeden Fall auch eine gewisse Geduld beim Üben haben, was zwar anstrengend ist, aber durch Erfolge ausgeglichen wird. Es ist für mich wichtig, zu versuchen, mit den Schülern auf Augenhöhe zu arbeiten, da Schüler und Lehrer genau gleich vor neuen Stücken anfangen müssen.

**Was ist der Unterschied zwischen dem Unterricht am Konservatorium und dem in der Schule?**

Man hat ein viel engeres Verhältnis und genaueres Kennenlernen mit dem Schüler beim Einzelunterricht, wobei das Vertrauen wirklich eine große Rolle spielt. Außerdem wird die Konzentration des Schülers länger und intensiver gefordert als in ganzen Klassen. So merken die Klavierspieler, dass sie üben müssen, um fit zu bleiben, genau wie beim Sport.

**Wie macht man es, dass der Unterricht abwechslungsreich bleibt?**

Man muss den Beruf ernst nehmen. Außerdem sind alle Schüler unterschiedlich - und das manchmal nicht zu knapp...

**Wie viele Stunden muss man als Instrumentallehrer in der Woche arbeiten?**

So viel, wie man für seinen Lebensunterhalt benötigt, wobei es bei Fest-und Privatanstellung auch wirklich große Unterschiede gibt.

**Beschäftigen Sie sich zuhause auch viel mit dem Klavierspielen?**

Ja, ich übe dort natürlich für Veranstaltungen und bereite mich auf den Unterricht mit den Schülern vor.

**Gab es bisher besonders amüsante Ereignisse im Zusammenhang mit dem Klavier?**

Ich hatte einmal ein Konzert, da fehlte auf dem Klavier eine Oktave. Ich war so verwundert, dass ich einfach die Töne in der Luft spielte und weitermachte.

**Frau Hagelstange, danke für dieses aufschlussreiche Gespräch!**

*Stella führte das Interview für die Tafelrunde.*

