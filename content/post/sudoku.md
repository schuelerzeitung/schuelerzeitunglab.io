+++
date = "2017-09-10T20:45:33+02:00"
description = "Hier findet ihr die Lösungen der Sudokus in Ausgabe 1"
title = "Lösungen der Sudokus - Ausgabe 1"
+++

Hier findet ihr die Lösungen der Sudokus in unseren gedruckten Ausgaben. Alle
Sudokus und deren Lösungen werden bereitgestellt von
[sudokuoftheday.com](http://sudokuoftheday.com). Vielen Dank!

# Leicht
![Lösungen des leichten Sudokus](/images/sudoku/ausgabe1/BeginnerSolution.png)
# Sehr Schwer
![Lösungen des sehr schweren Sudokus](/images/sudoku/ausgabe1/DiabolicalSolution.png)
