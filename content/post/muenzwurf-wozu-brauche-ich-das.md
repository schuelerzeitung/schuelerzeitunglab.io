+++
description = "Wozu brauch ich das? Eine Frage die wir uns alle stellen. Hier erklären wir einfache Phänomene mit Gelerntem aus der Schule."
date = "2017-06-29T14:36:01+02:00"
title = "Münzwurf: Wozu brauche ich das?"
tags = ["Wozu brauche ich das?"]

+++

Eine der einfachsten und bekanntesten Möglichkeiten, etwas zu entscheiden, ist der Münzwurf. Doch warum wird in dieser Rubrik ein Münzwurf behandelt? Ist es nicht so, dass die Wahrscheinlichkeit, oben zu liegen, für beide der Seiten genau 50% beträgt?

Kurz gesagt: Nein. 

Eine von Persi Diaconis, Susan Holmes und Richard Montgomery in Stanford durchgeführte Studie zu diesem Phänomen hat gezeigt, dass die Chancen, auf einer bestimmten Seite zu landen, in Wirklichkeit 51% und 49% betragen. Wenn du jetzt raten müsstest, welche was wäre, würdest du wahrscheinlich mit Kopf oder Zahl antworten -  allerdings ist es nicht ganz so.

Mit einer Wahrscheinlichkeit von 51% liegt die Seite oben, welche sich zum Beginn des Wurfes oben befand. 

Dies konnten die Wissenschaftler durch stundenlange Videoaufnahmen von Münzwürfen und die physikalische Analyse des Verhaltens der Münzen während des Fluges herausfinden.

## Und wie kann ich das nun für mich nutzen?
Die besten Chancen, bei einem Münzwurf zu gewinnen, bestehen, wenn man sowohl die Münze werfen, als auch eine Seite wählen darf.

Dies könnte euch einen Vorteil verschaffen, doch passt auch auf, wenn Andere dies mit euch tun. Solltet ihr nicht wählen und werfen dürfen, so sorgt wenigstens dafür, dass es fair bleibt *und* jede Partie jeweils eine Aufgabe übernimmt.

Als nächstes wählt ihr, um eure Chancen minimal zu verbessern, die Seite, die nicht zu sehen ist, also unten liegt. Dies hat den einfachen Grund, dass die Münze zwar mit einer marginal höheren Wahrscheinlichkeit auf der Seite landet, die zu Beginn oben war, es allerdings viele Menschen gibt, die vor der Veröffentlichung des Ergebnisses die Münze auf dem Handrücken noch einmal umdrehen. 

## Wenn schon ein Münzwurf nicht fair ist, was dann?

Viele werden jetzt nach Alternativen suchen, weil es ja anscheinend nicht so ist, dass 50/50 für beide Seiten gilt. Wenn man jetzt aber beachtet, dass diese Abweichung nur 2% ausmacht, sollte man sich nochmal überlegen, ob man das nicht einfach hinnimmt.  Immerhin bedeuten 2% einen von fünfzig Versuchen und, mal ehrlich, wer macht schon fünfzig Münzwürfe.

Allen, die trotzdem noch nach einer Alternative suchen, sage ich, dass das Drehen einer Münze auf dem Tisch auch keine Lösung ist, weil es hier so ist, dass die Münze auf der schwereren von beiden Seiten landet, und das mit einem noch höheren Unterschied als nur um 2%.

*Der Chefredakteur der Tafelrunde Erik Jenß geht dem Alltäglichen auf den Grund.*
