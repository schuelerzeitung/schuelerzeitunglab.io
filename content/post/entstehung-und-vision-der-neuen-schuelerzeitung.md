+++
date = "2017-05-29T20:59:03+02:00"
title = "Entstehung und Vision der neuen Schülerzeitung"
description = "K. Erik Jenß, unser Chefredakteur erzählt, wo die Tafelrunde herkommt und wo die Reise hingeht."
tags = ["Schulpolitik", "Schüler über Schüler", "Projekte"]
+++

Am Anfang stand eine Idee. Oder auch mehrere Ideen.

Okay, vielleicht stand am Anfang auch das, was von "ZehnNachDrei" übrig geblieben ist. Und darauf wollte ich aufbauen. Ich, K. Erik Jenß, wollte mit Hilfe von interessierten Schülern und Schülerinnen eine Schülerzeitung für alle an unserer Schule schaffen. 
Und anstatt von Ausgaben der Schülerzeitung hatte ich mich zuerst dafür entschieden, einen neuen Weg einzuschlagen: Wir hatten das Konzept entwickelt, eine Wandzeitung zu erstellen. 
Doch nach den ganzen Unsicherheiten bezüglich des Brandschutzes haben wir uns nun darauf geeinigt, dass wir einmalig in diesem Schuljahr Wandzeitungen mit den bisher besten Artikeln veröffentlichen, die ihr alle auch auf der Website lesen könnt.

# Wie kam es dazu?
Zu Beginn dieses Halbjahres wurde ich gefragt, ob ich mir nicht vorstellen könnte, die Leitung der Schülerzeitung zu übernehmen, nachdem die bisherige Leiterin nun auch ihren letzten Schultag hinter sich hat und im Abiturstress die AG und die Zeitung an sich nicht mehr leiten konnte. Dann hab ich verständlicherweise sehr gerne die Chance genutzt und das Steuer übernommen. Natürlich war mir da noch nicht bekannt, wie viel Arbeit das wirklich sein wird, aber das kam erst im Laufe der Zeit, nachdem ich der neue Leiter war. Es gab viele Entscheidungen zu treffen, viel Organisatorisches zu klären und viele der Überreste der vorherigen Schülerzeitungen zu strukturieren. 

# Neuer Name - was noch?
Nachdem viele von euch noch den Toaster kennen, wissen viele schon gar nicht, was es mit „ZehnNachDrei" auf sich hat. Für alle die es nicht wissen: Das war der neue Name für die Schülerzeitung in den letzten zwei Jahren. Doch ich habe mich entschlossen, diesen Namen auch nicht beizubehalten um einen Neustart mit der „Tafelrunde" zu wagen. 

„Tafelrunde" als Name ist gewählt worden, um eine Gemeinschaft darzustellen; wir möchten eine Runde von allen Lehrern und Schülern in der Schule schaffen, in der sich jeder äußern kann, wenn er möchte.
Auch ein neues Logo, welches eine Schreibfeder zeigt, haben wir uns angeeignet, welches dankenswerter Weise von Bjarne Wilhelm gestaltet wurde.

# Was steht in Zukunft so an?
Für die Zukunft planen wir, regelmäßig Artikel auf unserer Website zu veröffentlichen und beginnend mit dem nächsten Schuljahr möchten wir dann auch wieder gebundene Ausgaben verkaufen. Natürlich kann ich nicht alleine hier sitzen und alle Artikel selbst schreiben, deshalb hoffe ich, dass sich auch in Zukunft Schüler, Schülerinnen und gerne auch Lehrkräfte der Schule mit Artikeln an der „Tafelrunde" beteiligen möchten und bin auch glücklich, all diesen, und auch den Arbeitsgemeinschaften, die Möglichkeit geben zu können, sich auszudrücken. 

Für alle von Euch, die sich jetzt angesprochen fühlen; Ihr seid herzlich eingeladen, einen Artikel zu schreiben und uns zukommen zu lassen oder an unseren wöchentlichen Treffen montags nach der Schule teilzunehmen.
