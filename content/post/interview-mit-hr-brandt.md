+++
title = "Interview mit Hr. Brandt"
date = "2017-06-05T20:10:35+02:00"
tags = ["Interview"]
description = "Von vielen als Lehrer geschätzt und sogar zum Vertrauenslehrer gewählt. Die Tafelrunde interviewt Herrn Brandt."
+++
**Die Tafelrunde: Wie wichtig ist Ihnen Autorität gegenüber ihren Schülern?**

Hr. Brandt: Ganz wichtig, die Frage ist ja nur wie man diese Autorität schafft: durch Angst, Macht oder Verstehen und irgendwie auch durch Liebe. Ich hab irgendwo mal gelesen: „Pädagogik ist Vorbild und Liebe“ – das finde ich ganz gut. Und wenn das Schüler so merken, dann kriegt man automatisch auch Autorität.

**Würden Sie sagen, dass der Umgang mit Ihren Schülern auf Augenhöhe stattfindet?**

Nein. Das kann auch nicht funktionieren, weil es da immer dieses Gefälle zwischen Schüler und Lehrer gibt. Alles andere wäre Quatsch – also man kann als Schüler nicht befreundet sein mit einem Lehrer und umgekehrt.

**Ist der Kontakt mit Ihren Schülern also rein dienstlich oder auch privat?**

Also rein privat nicht, aber manchmal verwischt das ein bisschen, gerade über den Kontakt mit Whats App, da bin ich ja jemand, der sehr offen ist. Und dann ist es auch schon halb privat.

**Sie sind ja seit ein paar Monaten Vertrauenslehrer hier an der Schule. Wie hoch ist die Anfrage von Schülern zu diesem Angebot?**

Das ist ganz unterschiedlich. Ich hab manchmal einige Wochen, wo gar nichts ist, und gestern beispielsweise hatte ich vier Anfragen.

**Und die Schüler kommen dann in den Pausen selbst auf Sie zu?**

Ja, sie kommen spontan auf mich zu und ganz selten werde ich auch mal angeschrieben. 

**Und das dann über WhatsApp?**

Ja genau. Das hab ich extra auch so gewählt, weil es ein ganz niedrigschwelliges Angebot ist.

**Haben Sie denn dafür offizielle Sprechzeiten, in denen Sie zum Reden da sind?**

Also meistens ist es so, dass wir das spontan versuchen zu sortieren und zu klären, um gegebenenfalls nochmal einen anderen Termin zu machen.

**Haben Sie das Gefühl, dass die Schüler keine Probleme damit haben, offen mit Ihnen über ihre Probleme zu sprechen?**

Wenn sie sich auf mich zu bewegen, ist ja schon klar, dass sie gewillt sind, sich zu öffnen. Aber es ist sehr schwierig sich einem Fremden, und dann auch noch einem Lehrer, zu öffnen. Da hab ich großen Respekt vor den Schülern. 

**Und gerade an dem Punkt wird das Verhältnis zum Schüler auch privater oder nicht?**

Nein, ich bleibe dann Lehrer. Ich erzähle aber Geschichten, die aus meinem eigenen Leben berichten. Meine Erfahrung ist allerdings, dass ich vor allem der Zuhörer bin.

**Geben Sie Ihre Ratschläge dann als eher aus der Position des Lehrers oder als Privatperson?**

Ja beides. Als Lehrer natürlich zunächst, wenn Schüler Probleme mit Kollegen haben. Dann rate ich natürlich vorerst zum dienstlichen Weg. Es fließt aber auch einiges Privates mit hinein in meine Ratschläge.

**Verändert sich Ihr Bild der Schüler/innen, nachdem sie bei Ihnen im Gespräch waren?**

Nein. In den Schülern getäuscht hab ich mich bis jetzt selten, aber das hat auch keinen Einfluss auf meine Beurteilung als Lehrer auf den Schüler, und schon gar nicht auf die Benotung. Das sind zwei verschiedene Dinge, die man trennen muss.

**Gehen Ihnen die Angelegenheiten und Probleme der Schüler/innen persönlich nahe?**

Generell versuche ich das schon zu trennen. Wenn ich mal nicht weiterkomme, bespreche ich das allerdings mit meiner Freundin oder meiner Frau. Dann schildere ich die Angelegenheit jedoch verzerrt, damit es nicht wiederzuerkennen ist. Es geht ja dann nur um das Allgemeine. Die Dinge, die ich nicht verstehe, sind aber stetig die gleichen; also wie kann es sein, dass Eltern ihre Kinder so behandeln oder eben nicht behandeln. Das ist etwas, das mich wirklich bewegt. Allerdings gilt das auch für Lehrer, dass ich mich manchmal frage, wie man Schülern so begegnen kann.

**Aber Sie gehen dann auch nicht außerhalb dieser Sitzungen beispielsweise auf die Lehrer zu?**

Nein, das wäre zu offensichtlich und außerdem darf ich das ja gar nicht. Das versuche ich dann schon geschickter zu lösen. Oberstes Gebot ist natürlich, dass sich die Schüler darauf verlassen können, dass das Gesagte auch bei mir bleibt.

**Würden Sie sagen, dass Sie zu Ihren Schülern eine persönliche Bindung entwickeln, oder sehen Sie sich nur als Vermittler des Unterrichtsstoffs?**

Nein, das wisst ihr ja auch. Ich gehe immer in Beziehung zum Schüler. Ich sehe nicht die Schüler vor mir, sondern tatsächlich die einzelnen Menschen.

**Vielen Dank für das Interview.**

*Das Interview führen Christoph Combes und Joel Marfo.*

