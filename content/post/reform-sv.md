+++
date = "2017-09-04"
title = "Das Schülerplenum - Reform der Schülervertretung"
tags = ["Schulpolitik"]
description = "Eine Umstrukturierung soll mehr Schüler in die Schülervertretung einbinden. Wie das funktioniert erfahrt ihr in diesem Artikel."
+++

*Ben J. Bals ist Schülersprecher und berichtet in diesem Artikel über die Reform der Schülervertretung.*

Was ist die Aufgabe einer Schülervertretung? Ganz einfach: die Schüler zu vertreten. Doch das ist nicht so leicht wie es klingen mag und um diese Aufgabe besser erfüllen zu können hat die Schülervertretung am 16. Juni 2017 eine Änderung der Satzung beschlossen.

## Worum geht’s da?
Ziel der Änderung ist es, möglichst viele Schüler aller Klassenstufen in die Arbeit und besonders in die Entscheidungsfindung der Schülervertretung einzubinden. Außerdem dient sie dem Zweck, möglichst akkurat, häufig und detailliert wichtige Informationen an die Schüler weiterzugeben und eine offene Zwei-Wege-Kommunikationskultur aufzubauen.

## Wie war’s bis jetzt?
Bis jetzt bestand die Schülervertretung aus 9 Klassenstufenvertretern, einem Vertreter des Internats, dem Schülersprecher und seinem Stellvertreter. Diese 12 Schüler waren bei Beschlüssen stimmberechtigt. Außerdem haben in der Schülervertretung noch nicht-stimmberechtigte Beauftragte und Beisitzer mitgearbeitet.

In dieser kleinen Runde aus ca. 18 Schülern konnten wir uns gut koordinieren und schnell zusammenarbeiten. Dafür fällt es natürlich so wenigen schwer, so viele (über 1200) akkurat zu repräsentieren. Daher haben wir uns entschieden, eine größere Gruppe von Schülern in unsere Arbeit einzubinden.

## Wie ist’s jetzt?
Ab sofort besteht die Schülervertretung aus drei Organen: dem Schülerplenum, dem Schülerrat und dem Präsidium.

### Das Schülerplenum
Das Schülerplenum besteht aus allen Klassensprechern, zwei pro Klasse. Es umfasst also über 100 Schüler. Das Schülerplenum trifft sich mindestens vier mal im Schuljahr: je nach den Sommer-, Herbst-, Weihnachts-, und Osterferien.

Das Schülerplenum hat im wesentlichen drei Aufgaben: Entscheidungen treffen, Informationen an die Klassensprecher geben und Fragen der Klassensprecher beantworten.

Das Schülerplenum ist, wie die anderen Organe, berechtigt, Entscheidungen im Namen der Schülervertretung zu treffen. Im Schülerplenum haben alle Schüler die Möglichkeit Beschlüsse zu Abstimmung zu stellen.

Außerdem dient das Schülerplenum als Zwei-Wege-Sprachrohr zwischen dem Schülerrat und den Klassensprechern. Der Schülerrat gibt wichtige Informationen an die Klassensprecher und die Klassensprecher haben die Möglichkeit, alle ihre Fragen zu stellen.

### Der Schülerrat

Der Schülerrat ist das, was vorher die ganze SV war. Er hat die gleichen 12 stimmberechtigten Mitglieder und ihre Beauftragten und Beisitzer. 

Der Schülerrat trifft sich häufiger als das Schülerplenum: ca. monatlich. Er diskutiert weniger wichtige Themen für die in einer Runde wie dem Schülerplenum keine Zeit bleibt. Außerdem ist er durch seinen schlankeren Aufbau agiler und kann schneller auf auftretende Situationen reagieren.

Der Schülerrat veröffentlicht, wie das Schülerplenum, seine Protokolle und Beschlüsse auf der Website der Schülervertretung.

### Das Präsidium
Das Präsidium besteht aus dem Schülersprecher, seinem Stellvertreter und dem Vorsitzenden des Schülerrats. Es dient quasi als ausführende Kraft und vertritt die Schülervertretung in anderen Gremien der Schule und auf offiziellen Anlässen.

## Noch Fragen?
Die genauen Regelungen findest Du in unserer Satzung unter sv.cjd-rostock.de/infos und natürlich kannst du mir jederzeit eine E-Mail an ben.sv-rostock@cjd-nord.de schreiben.
