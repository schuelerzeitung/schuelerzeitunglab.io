+++
date = "2017-05-19T08:31:07+02:00"
tags = ["Schulpolitik"]
description = "Wieso wird das gemacht und was steht da *wirklich* drin?"
title = "Die neue Handy-Ordnung"
+++

# Die neue Handy-Ordnung

*Dieser Artikel wurde von Ben J. Bals, Mitglied der Schülervertretung geschrieben. Er war an der Erarbeitung beteiligt. Bitte beachtet das bei Eurer Bewertung.*

Der Punkt Drei der Hausordnung, „Umgang mit Mobilcomputern”, oder schlicht Handy-Ordnung, regelt, wie und wann Schülerinnen und Schüler ihre Handys, Laptops, Tablets etc. verwenden dürfen.

## Was darf ich und was darf ich nicht?

Die Grundzüge der Ordnung sind einfach. In den Klassen 5 bis 9 ist die Verwendung gänzlich untersagt, in den Klassen 10 bis 13 ist sie in den Pausen gestattet. Eine Ausnahme bildet die Essenausgabe in der Mensa, dort ist die Verwendung von Mobilcomputern grundsätzlich aus Gründen der Höflichkeit verboten. Fotos und Videos sind nur mit ausdrücklicher Erlaubnis anzufertigen.

Alles in allem sind die Regelungen einfach und nicht wirklich spannend. Und das ist auch gut so. Bei der Erarbeitung hat die zuständige Arbeitsgruppe großen Wert auf Einfachheit gelegt. Und hier beginnt der spannende Teil der Geschichte.

## Wie ist die Regelung entstanden?

Die Arbeitsgruppe besteht aus Lehrern, Eltern, Schulleitung und, erstmals, Schülern. Bei den Sitzungen der AG saßen zwei Mitglieder der Schülervertretung, Wencke Kretzschmar und ich, Ben Bals, mit am Tisch. Dort haben wir die Interessen der Schülerinnen und Schüler vertreten und für eine faire und praktikable Lösung gekämpft.

Beschlossen wurde die Regelung am 28. März auf der Lehrerkonferenz. Dort wurde auch der Zusatz bezüglich der Mensa vorgeschlagen und verabschiedet.

Fair, praktikabel und einfach waren auch die Grundsätze nach denen über die neue Konsequenz bei Verstößen entschieden wurde. Alle Mitglieder der Arbeitsgruppe waren sich einig, dass das Einziehen von Geräten zum einen unpraktisch und ein logistischer Aufwand ist und zum anderen aber auch unfair. Also haben wir gemeinsam nach einem Ansatz gesucht, der das erzieherische Gespräch und die Vermittlung der Sinnhaftigkeit der Regelung in den Mittelpunkt stellt.

## Was passiert bei einem Verstoß?

So sind wir darauf gekommen die Eltern über Verstöße via E-Mail zu benachrichtigen, beim ersten Verstoß mit der Bitte dem Kind die pädagogischen Hintergründe zu erläutern, beim zweiten Mal mit der Einladung zu einem Gespräch zwischen Eltern, Kind und Klassenleiter. Die Klassenleiter werden ebenfalls über jeden Verstoß informiert. Die Zählung der Verstöße erfolgt auf Halbjahresbasis.

Gemeldet werden Verstöße in einer eigens dazu entwickelten App oder Website. Der Quelltext steht öffentlich zur Verfügung unter gitlab.com/phone\_monitoring. Wir laden alle technisch Versierten dazu ein ihn sich anzusehen. Datensicherheit schreiben wir groß und so setzten wir nur Industriestandards ein.

## Fragen?

Bei Fragen könnt Ihr mich gerne einfach ansprechen oder mir eine E-Mail an [ben.sv-rostock@cjd-nord.de](mailto:ben.sv-rostock@cjd-nord.de) schicken.
