+++
date = "2017-04-30T21:06:17+02:00"
title = "Interview mit CJD Starterpacks"
description = "Der wohl kontroverseste zeitgenössische Künstler, *CJD Starterpacks*, hat seine Seele der Tafelrunde geöffnet."
tags = ["Schüler über Schüler", "Interview"]
+++

Kaum ein Künstler hat in der letzten Zeit die Gemüter so sehr erhitzt wie „CJD Starterpacks“. Durch die Anonymität geschützt verbreitet er seine Kunst in den sozialen Medien und erntet viel Lob, aber auch gleichermaßen viel Kritik. Der Künstler schafft es, die Realität auf simpelste Eindrücke herunterzubrechen und ist dabei genial. Wir haben es geschafft, dass uns dieses gesichtslose Genie die ein oder andere Frage beantwortet.

**Tafelrunde: Sie schützen sich durch Anonymität, was gewinnen Sie als Künstler daraus?**

CJD Starterpacks: Durch meine Anonymität gewinne ich die Freiheit, das zu schreiben, was ich denke. Ich muss also keine Angst davor haben, dass Leute mir vorschreiben, etwas falsch gemacht zu haben, oder dass es mir persönlich übel genommen wird.

**Wie viele Personen wissen um Ihre Identität? Haben Sie manchmal das Gefühl, sich outen zu wollen, damit Sie die Lorbeeren kassieren können?**

Mit mir sind nur zwei weitere Personen aus der Schule involviert. Diese Seite zu führen, hat schon etwas Aufregendes; mit anderen darüber zu sprechen, die nicht wissen, dass ich diese Seite führe. Ich denke nicht, dass es positive Auswirkungen haben wird, wenn ich mich outen würde. Es gibt viele Menschen, die unzufrieden mit meiner Seite beziehungsweise meinem Content sind. Die Seite ist immerhin Gesprächsthema an der Schule und das macht mich trotzdem ein wenig stolz.

**In Ihren frühen Werken erkennt man deutlich eine Zerrissenheit; inwiefern spiegelt dies Sie selbst als Menschen wieder?**

Zu einem künstlerischem Wesen gehört eine gewisse Zerrissenheit schon dazu, denn nicht alles kann immer gradlinig sein und einige Fehler (z. B. dass Leute unzufrieden mit meiner Kunst sind) sind auch nicht zu vermeiden.

**Ihr Werk „Herr Brandt Starterpack“ wird von vielen als Meisterwerk gesehen, aber auch ebenso kritisiert. Wie gehen Sie mit Kritik um und inwiefern beeinflusst sie Ihre Kunst?**

Mit der Kritik kann ich gut umgehen, denn auch negative Kritik ist gute Werbung. Wichtig ist, die Leute mit meiner Kunst gut zu unterhalten, und mögliche Diskussionen anzuregen.

**Was begeistert Sie so an ihrer Kunstform, der Instagram-Fotocollage?**

Es macht Spaß, sich mit einem Thema beziehungsweise einer Person genauer zu beschäftigen, Kleinigkeiten groß zu machen, und auch mal den Finger in die Wunde zu legen. Außerdem profitieren einige Schüler davon, da sie den Fame bekommen, den alle irgendwo ein bisschen wollen.

**Wenn der Feuilleton auf einen jungen Künstler aufmerksam wird, dauert es in der Regel nicht lange bis zu den ersten großen Ausstellungen. Wann können wir das bei ihnen erwarten?**

 Da es momentan schwierig ist, die Seite aktiv zu führen, da die Ideen immer mehr ausgehen, denke ich nicht, dass eine Ausstellung ansteht. Zumindest nicht in nächster Zeit. Ich bir für neue Ideen und Themen natürlich immer offen.

*Das Interview führte Elias Westphal.*
