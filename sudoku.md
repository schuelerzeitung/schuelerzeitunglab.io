# Lösungen der Sudokus
Hier findet ihr die Lösungen der Sudokus in unseren gedruckten Ausgaben. Alle
Sudokus und deren Lösungen werden bereitgestellt von
[sudokuoftheday.com](http://sudokuoftheday.com). Vielen Dank!

## Ausgabe 1
### Leicht
![Lösungen des leichten Sudokus](images/sudoku/ausgabe1/BeginnerSolution.png)
### Sehr Schwer
![Lösungen des sehr schweren Sudokus](images/sudoku/ausgabe1/DiabolicalSolution.png)
